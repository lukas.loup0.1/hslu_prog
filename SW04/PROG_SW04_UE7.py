#Das Program soll zeigen wie man Schön eine Tabelle darstellen kann

def fahrenheit(celsius):
    return (9 / 5) * celsius + 32


print(f'Celsius{"Fahrenheit":>14}')

for celsius in range(101):
    print(f'{celsius:7}{fahrenheit(celsius):14.1f}')