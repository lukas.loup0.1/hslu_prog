# account.py
"""Account class definition."""
from decimal import Decimal

class Account:
    """Account class for maintaining a bank account balance."""
    
    def __init__(self, name, balance):
        """Initialize an Account object."""
        
        # if balance is less than 0.00, raise an exeption
        if balance < Decimal('0.00'):
            raise ValueError('Initial balance must be >= to 0.00')
        
        self.name = name
        self.balance = balance
        
    def deposit(self, amount):
        """Deposit money to the account."""

        # if amount is less than 0.00, raise an exception
        if amount < Decimal('0.00'):
            raise ValueError('amount must be positive.')

        self.balance += amount

    def withdraw(self, deduct):
        print('balenc', self.balance, 'deduct', deduct)

        if deduct <  Decimal('0.00'):
            raise ValueError('amount must be positive.')
        elif self.balance < deduct:
            raise ValueError('not enough money.')
        else:
            self.balance -= deduct
            

    @property
    def name(self):
        """Return the name."""
        return self._name
        
    @name.setter
    def name(self, name):
        """Set the name."""
        self._name = str(name)

    @property
    def balance(self):
        """Return the balance."""
        return self._balance

    @balance.setter
    def balance(self, balance):
        """Set the name."""
        self._balance = Decimal(balance)


if __name__ == "__main__":

    user1= Account('Lukas',Decimal(50))
    print(user1.balance)
    user1.withdraw(10)
    print(user1.balance)
    user1.deposit(20)
    print(user1.balance)

 