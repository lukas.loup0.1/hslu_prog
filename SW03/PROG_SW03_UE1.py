passes = 0
failures = 0
failinput = 0
result = 0

for student in range(10):
    while not(result == 1) and not(result == 2):
        try:
            print(result)
            result = int(input('Enter the result (1 = passed, 2 = failed): '))
        except:
            print ('Not a Number')
            failinput += 1
            continue
    print ('out of while')
    
    if result == 1:
        passes += 1
    elif result == 2:
        failures += 1
    else:
        failinput += 1
    result = 0
print('passes:', passes)
print('failures:', failures)
print('failinput:', failinput)