'''Aufgabe 1
Modifizieren Sie die Klasse Accounts im Modul account wie folgt:

Benennen Sie die Instanzvariablen so um, dass sie mit einem Underscore (_) beginnen,
um gemäss Python Konvention anzudeuten, dass diese Variablen nur intern benutzt werden sollen.
Fügen Sie read-only Properties hinzu um die Werte in den Instanzvariablen zu lesen.
Fügen Sie eine Methode withdraw hinzu, um Geld vom Konto abzuheben. Stellen Sie dabei sicher,
dass das Konto nicht überzogen werden kann. Überprüfen Sie auch, dass das der Bezugsbetrag positiv ist.
Testen Sie anschliessend Ihre aktualisierte Klasse.

Sind die umbenannten Instanzvariablen nun schreibgeschützt? Versuchen Sie ihnen Werte direkt zuzuweisen.'''


from account import Account
from decimal import Decimal

user1= Account('Lukas',Decimal(50))
print(user1.balance)
user1.withdraw(10)
print(user1.balance)
user1.deposit(20)
print(user1.balance)

 

'''Fügen Sie der Klasse Time im Modul timewithproperties ein read-only Property universal_str hinzu,
das eine String-Repräsentation von Time im 24h-Format mit jeweils zwei Ziffern für Stunden, Minuten und Sekunden zurückgibt. 
Zum Beispiel gibt die Methode universal_str einen String '06:30:00' zurück. Testen Sie Ihre aktualisierte Klasse.'''

from timewithproperties import Time

clook = Time(1,30,12)
print(clook.universal_str)

'''Modifizieren Sie die interne Repräsentation der Klasse Time so, dass die Zeit intern als Sekunden seit Mitternacht in einer Variable _total_seconds gespeichert wird.
Ersetzten Sie daher die bestehenden Attribute _hour, _minute und _second durch _total_seconds.
Passen Sie anschliessend die bestehenden Properties und Methoden an, damit die Zeit nach wie vor wie bisher gesetzt, oder ausgegeben werden kann.
Führen Sie die folgenden Statements einmal mit der ursprünglichen Time Klasse und einmal mit der aktualisierten Time Klasse aus,
um zu ziegen, dass die beiden Klassen austauschbar sind.'''

'''
Ein Kreis hat einen Punkt in seiner Mitte.
Erstellen Sie eine Klasse Point, die ein (x, y)-Koordinatenpaar darstellt und x- und y- Lese-/Schreib-Properties für die Attribute _x und _y bereitstellt.
Implementieren Sie auch die Methoden __init__ und __repr__, sowie eine Methode move, 
welche x- und y- Koordinatenwerte empfängt und die neue Position des Punktes festlegt.
Erstellen Sie dann eine Klasse Circle, deren Attribute _point (ein Punkt, der den Mittelpunkt des Kreises darstellt) und _radius sind.
Implementieren Sie die Methoden __init__ und __repr__ sowie eine Methode move,
die x- und y-Koordinatenwerte empfängt und eine neue Position für den Kreis festlegt, indem sie die move Methode ihres Point-Objekts aufruft.
Testen Sie Ihre Circle-Klasse, indem Sie ein Circle-Objekt erstellen,
seine Repräsentation (durch impliziten Aufruf der __repr__ Methode) anzeigen, anschliessend den Kreis verschieben und seine Repräsentation erneut anzeigen.'''