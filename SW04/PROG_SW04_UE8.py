'''Schreiben Sie ein Script, das "Errate die Zahl" spielt.
Wählen Sie die zu erratende Zahl, indem Sie eine zufällige ganze Zahl im Bereich von 1 bis 1000 wählen.
Geben Sie diese Zahl der Spielerin oder dem Spieler nicht preis.
Zeigen Sie die Eingabeaufforderung "Erraten Sie meine Zahl zwischen 1 bis 1000" an.
Die Spielerin oder der Spieler gibt nun eine erste Schätzung ein.
Wenn die Schätzung falsch ist, zeigen Sie entweder "Zu hoch. Versuchen Sie es noch einmal.",
oder "Zu tief. Versuchen Sie es noch einmal." an und fordern zur nächsten Schätzung auf.
Wenn die Spielerin oder der Spieler die richtige Antwort eingibt,
zeigen Sie "Gratulation, Sie haben meine Zahl erraten!" an und lassen Sie die Spielerin oder den Spieler wählen,
ob sie oder er erneut spielen möchte.'''

import random

def user_input(min_nr, max_nr):
    try:
        ges_nr = int(input('Erraten Sie meine Zahl zwischen ' + str(min_nr) + ' bis ' + str(max_nr)+' : '))        
        return ges_nr
    except:
        print('Das wahr keine Zahl!!')
        agian = input('Wotsch nomau Probiere e Zahl i ge?')
        if agian == 'ja' or agian == 'JA' or agian == 'Yup':
            ges_nr = user_input(min_nr,max_nr)
            return ges_nr
        else:
            return 

def my_nr(min_start_nr= 0, max_star_nr=1000):
    nr_to_ges = random.randrange(min_start_nr,max_star_nr)
    return nr_to_ges

def ceck_nr(nr_to_ges, ges_nr,min_nr,max_nr,min_start_nr=0, max_star_nr=1000):
    if nr_to_ges == ges_nr:
        print('Gratulation, Sie haben meine Zahl erraten!')
        agian = input('No es Spiel(ja)?')
        if agian == 'ja' or agian == 'JA' or agian == 'Yup':
            nr_to_ges = my_nr()
            min_nr, max_nr = min_start_nr, max_star_nr
            ges_nr = user_input(min_nr,max_nr)
            ceck_nr(nr_to_ges, ges_nr,min_nr,max_nr)
        return    
    elif nr_to_ges < ges_nr:
        print("Zu hoch. Versuchen Sie es noch einmal.")
        max_nr = ges_nr
        print(nr_to_ges)
        ges_nr = user_input(min_nr, max_nr)
        ceck_nr(nr_to_ges, ges_nr,min_nr,max_nr)

    elif nr_to_ges > ges_nr:
        print("Zu tief. Versuchen Sie es noch einmal.")
        min_nr = ges_nr
        print(nr_to_ges)
        ges_nr = user_input(min_nr, max_nr)
        ceck_nr(nr_to_ges, ges_nr,min_nr,max_nr)

if __name__ == "__main__":
    min_nr,max_nr = 0, 1000
    nr_to_ges = my_nr()
    ges_nr = user_input(min_nr, max_nr)
    ceck_nr(nr_to_ges, ges_nr, min_nr, max_nr)