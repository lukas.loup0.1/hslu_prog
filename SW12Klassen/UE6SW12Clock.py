class NumberDisplay:
    '''Clocki bli bla bla '''

    def __init__(self,time=0,maxSteps=60):
        self.time = time
        self.maxSteps = maxSteps

    @property
    def time(self):
        return self._time
    @time.setter
    def time(self,time):
        self._time = int(time)
    @property
    def maxSteps(self):
        return self._maxSteps        
    @maxSteps.setter
    def maxSteps(self,max):
        max = int(max)
        if max > 100:
            raise ValueError('Max Steps have to be smaller than 100')
        self._maxSteps = max

    def display(self):
        if self.time >= self.maxSteps:
            self.time = 0
        if self.time < 10:
            return f'0{self.time}'
        return str(self.time)

    def time_tick(self):
        self.time += 1

if __name__ == "__main__":
    h = NumberDisplay(1,24)
    min = NumberDisplay(2,60)
    for i in range(60):
        min.time_tick()
        print(f'{h.display()}:{min.display()}')

