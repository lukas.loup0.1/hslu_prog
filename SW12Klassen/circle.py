
'''

    Klasse Name : ->  Circle
    Methoden __init

'''
class Circle:
    def __init__(self, x=0, y=0, r=0):
        """Initialize each attribute."""
        self.x = x
        self.y = y
        self.r = r

    @property
    def x(self):
        '''Reurn the X '''
        return self._x
    @x.setter
    def x(self, x):
        """Set the x."""
        self._x = x
    @property
    def y(self):
        '''Reurn the y '''
        return self._y
    @y.setter
    def y(self, y):
        """Set the y."""
        self._y = y
    @property
    def r(self):
        '''Reurn the R '''
        return self._r
    @r.setter
    def r(self, r):
        """Set the r."""
        if 0 > r:
            r *= -1
        self._r = r

    def move(self,x=0,y=0):
        '''Move Point'''
        self.x = x
        self.y = y 

    def __repr__(self):
        """Return Circle string for repr()."""
        return (f'Circle(Y_Achse: {self.y}, X_Achse: {self.x}, ' + 
                f'Radius: {self.r})')
if __name__ == "__main__":
    cs = Circle(2,3,-5)
    print(cs)
    cs.move(10,12)
    print(cs)
    cs.move(-10,-20)
    print(cs)
