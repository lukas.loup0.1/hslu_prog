# timewithproperties2.py
"""Class Time with read-write properties."""

class Time:
    """Class Time with read-write properties."""   
    def __init__(self, hour=0, minute=0, second=0):
        """Initialize each attribute."""
        self.total_seconds=(hour, minute, second)

    def spilt(self,tsec):
        '''Spilt Total sec in h:min:sec'''
        hr = tsec/3600
        #print('hr:  ', hr)
        h = int(hr)
        #print('h:  ', h)           
        minr = (hr-h)*60
        #print('minr:  ',minr)
        min = int(minr)
        #print('min:  ', min)
        sec = int((minr-min)*60)
        #print('sec:   ',sec)
        return [h,min,sec]

    @property
    def total_seconds(self):
        """Return the hour."""
        return self._total_seconds

    @total_seconds.setter
    def total_seconds(self,tub_h_min_sec):
        """Set the hour.""" 
        h,min,sec, = tub_h_min_sec
        if h >= 24 or h < 0:
            raise ValueError(f'Hour ({hour}) must be 0-23')   
        self._total_seconds = h*60**2 + min*60 + sec
    
    @property
    def universal_str(self):
        hour, minute, second = self.spilt(self.total_seconds)
        return f'{hour:>2}:{minute:0>2}:{second:0>2}' 
        
    
    def set_time(self, hour=0, minute=0, second=0):
        """Set values of hour, minute, and second."""
        self.total_seconds = hour, minute, second
    

    def __repr__(self):
        """Return Time string for repr()."""
        return (f'Time(hour={self.hour}, minute={self.minute}, ' + 
                f'second={self.second})')
    
    def __str__(self):
        """Print Time in 24-hour clock format."""
        return (('12' if self.hour in (0, 12) else str(self.hour % 12)) 
                + f':{self.minute:0>2}:{self.second:0>2}' 
                + (' AM' if self.hour < 12 else ' PM'))

    # Implementieren Sie hier die Lösung




if __name__ == "__main__":

    clook = Time(0,30,12)
    print(clook.universal_str)