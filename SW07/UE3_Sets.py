'''Aufgabe 3
Ergänzen Sie den fehlenden Code. Ersetzen Sie in jedem der folgenden
Ausdrücke die *** durch einen Set-Operator,
 der das im Kommentar angezeigte Ergebnis erzeugt.
Geben Sie für jeden der ersten vier Ausdrücke den Namen der 
Mengenoperation an, die das angegebene Ergebnis liefert.
Die letzte Operation in Teilaufgabe e) sollte prüfen, 
ob der linke Operand eine unechte Teilmenge des rechten Operanden ist.
a) {1, 2, 4, 8, 16} *** {1, 4, 16, 64, 256} # {1,2,4,8,16,64,256}
b) {1, 2, 4, 8, 16} *** {1, 4, 16, 64, 256} # {1,4,16}
c) {1, 2, 4, 8, 16} *** {1, 4, 16, 64, 256} # {2,8}
d) {1, 2, 4, 8, 16} *** {1, 4, 16, 64, 256} # {2,8,64,256}
e) {1, 2, 4, 8, 16} *** {1, 4, 16, 64, 256} # False'''


print({1, 2, 4, 8, 16}.union({1, 4, 16, 64, 256})) # {1,2,4,8,16,64,256} .union or |
print({1, 2, 4, 8, 16} & {1, 4, 16, 64, 256}) # {1,4,16} & or .intersection
print({1, 2, 4, 8, 16} - {1, 4, 16, 64, 256}) # {2,8} - or .difference
print({1, 2, 4, 8, 16} ^ {1, 4, 16, 64, 256}) # {2,8,64,256} ^ or .symmetric_difference
print({1, 2, 4, 8, 16}.isdisjoint({1, 4, 16, 64, 256})) # 