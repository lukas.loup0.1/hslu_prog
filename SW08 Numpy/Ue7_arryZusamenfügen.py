import numpy as np

arry1 = np.arange(0,4).reshape(2,2)
arry2 = np.arange(4,8).reshape(2,2)
arry3 = np.append(arry1,arry2,axis=0)
arry4 = np.append(arry1,arry2, axis=1)
arry5 = np.append(arry4,arry4,axis=0)
arry6 = np.append(arry3,arry3,axis=1)

print(arry1)
print()
print(arry2)
print()
print(arry3)
print()
print(arry4)
print()
print(arry5)
print()
print(arry6)
print()
print(np.zeros((4,4)))